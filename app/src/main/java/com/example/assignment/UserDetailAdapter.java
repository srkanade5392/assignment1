package com.example.assignment;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


import com.example.assignment.databinding.UserDetailsRowListPdfBinding;

import java.util.List;

public class UserDetailAdapter extends RecyclerView.Adapter<UserDetailAdapter.UserDetailViewHolder> {
    private MainActivity activity;
    private UserDetailsRowListPdfBinding binding;
    List<UserDetails> resultList;

    public UserDetailAdapter(List<UserDetails> resultList, MainActivity activity) {
        this.resultList = resultList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public UserDetailAdapter.UserDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.user_details_row_list_pdf, parent, false);

        return new UserDetailAdapter.UserDetailViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull UserDetailAdapter.UserDetailViewHolder holder, int position) {
        holder.binding.textviewFname.setText(resultList.get(position).getFirst_name());
        holder.binding.textviewLname.setText(resultList.get(position).getLast_name());
        holder.binding.textviewGender.setText(resultList.get(position).getGender());
    }

    @Override
    public int getItemCount() {
        return resultList.size();
    }

    public class UserDetailViewHolder extends RecyclerView.ViewHolder {
        private UserDetailsRowListPdfBinding binding;

        public UserDetailViewHolder(UserDetailsRowListPdfBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
