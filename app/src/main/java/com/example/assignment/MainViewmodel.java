package com.example.assignment;

import android.util.Log;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.assignment.databinding.ActivityMainBinding;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainViewmodel {
    private MainActivity activity;
    private ActivityMainBinding binding;
    private UserDetailAdapter adapter;
    RequestQueue mRequestQueue;


    public MainViewmodel(MainActivity mainActivity, ActivityMainBinding binding) {
        this.activity = mainActivity;
        this.binding = binding;
    }

    public void onCallJsonDataClick() {
        mRequestQueue = Volley.newRequestQueue(activity);
        String url = "https://gorest.co.in/public-api/users?access-token=oUYffl4fr0UovgfBAXiA3yxp0bd5zpupRyNi";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("result");
                    List<UserDetails> resultList = new ArrayList<>();
                    for (int index = 0; index < jsonArray.length(); index++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(index);
                        Gson gson1 = new Gson();
                        UserDetails results = gson1.fromJson(jsonObject.toString(), UserDetails.class);
                        resultList.add(results);
                    }
                    setUserDetailsAdapter(resultList);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", error.toString());
            }
        });
        mRequestQueue.add(jsonObjectRequest);
    }

    private void setUserDetailsAdapter(List<UserDetails> resultList) {
        adapter = new UserDetailAdapter(resultList, activity);
        binding.recyclerviewUserdetais.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        binding.recyclerviewUserdetais.setLayoutManager(mLayoutManager);
        binding.recyclerviewUserdetais.setItemAnimator(null);
        binding.recyclerviewUserdetais.setAdapter(adapter);
    }
}
